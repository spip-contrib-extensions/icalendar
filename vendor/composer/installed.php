<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'reference' => NULL,
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'reference' => NULL,
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'kigkonsult/icalcreator' => array(
            'pretty_version' => 'v2.41.80',
            'version' => '2.41.80.0',
            'reference' => 'a15c9b48caa2b50e077f5a9595f889c965d58667',
            'type' => 'library',
            'install_path' => __DIR__ . '/../kigkonsult/icalcreator',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
