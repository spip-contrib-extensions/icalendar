# Changelog

## 1.0.0 - 2023-07-02


### Changed

- Passage à la v2.41.80 de la librairie icalcreator:
  * Plus les mêmes casses (et donc classes)
  * Beaucoup de propriété sont désormais privées
  * Il faut désormais employer les getteurs
- Pour l'itérateur:
	* Les dates sont renvoyées au format Sql, et plus sous la forme d'un tableau
	* Les autres propriétés sont accessibles directement (`propriete`), sans aller chercher sous `propriete/x/val`
	* Certaines propriétés ne sont plus tabulaires (dépend de la librairie)
- Nécessite PHP 8.0 et l'extension PHP `intl`
## 0.5.13 - 2023-07-02

### Fixed

- Pas de compatibilité SPIP 8 (c'est la v1 du plugin qui est compatible), on s'assure d'être en PHP 7
## 0.5.11 - 2023-06-12

### Fixed

- validité partielle PHP 8.1
- utiliser inc/distant de SPIP pour récupérer correctement un calendrier Outlook

## 0.5.9 - 2022-05-26


### Fixed

- Les requêtes direction les FLUX Facebook marchent à nouveau


