<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
# hors de la fonction, de facon a ce que la class soit chargee
# meme si le resultat est deja dans le cache (sinon le cache est inexploitable).
# cf. iterateur/data.php

include_once(_DIR_PLUGIN_ICALENDAR . '/vendor/kigkonsult/icalcreator/autoload.php');
include_spip('inc/distant');


function inc_ics_to_array($u) {
	static $getters = [];
	if (!$getters) {
		$methods = get_class_methods('Kigkonsult\Icalcreator\Vevent');

		// Ne trouver que les méthodes qui commencent par get ou getAll et qui n'est pas getProperties
		foreach ($methods as $m) {
			if ($m === 'getProperties') {
				continue;
			}
			if (substr($m, 0, 6) === 'getAll') {
				$getters[strtolower(substr($m, 6))] = $m;
				continue;
			}
			if (substr($m, 0, 3) === 'get') {
				$get_all = str_replace('get', 'getAll', $m);
				if (!in_array($get_all, $methods)) {
					$getters[strtolower(substr($m, 3))] = $m;
					continue;
				}
			}
		}
	}
	$cal = new \Kigkonsult\Icalcreator\Vcalendar();
	$cal->parse($u);

	$return = [];
	foreach ($cal->getComponents(\Kigkonsult\Icalcreator\Vcalendar::VEVENT) as $k => $v) {
		$event_properties = [];
		foreach ($getters as $prop => $getter) {
			$get = $v->$getter();
			if (gettype($get) === 'object' && get_class($get) === 'DateTime') {
				$get = $get->format('Y-m-d H:i:s');
			}
			$event_properties[$prop] = $get;
		}
		$return[] = $event_properties;
	}
	return $return;
}
